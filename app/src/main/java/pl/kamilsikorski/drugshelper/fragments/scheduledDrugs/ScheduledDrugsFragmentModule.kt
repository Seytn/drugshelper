package pl.kamilsikorski.drugshelper.fragments.scheduledDrugs

import dagger.Module
import dagger.Provides
import pl.kamilsikorski.drugshelper.repositories.TasksRepository

/**
 * Created by ksikorski on 16.10.2017.
 */
@Module
class ScheduledDrugsFragmentModule {
    @Provides
    internal fun provideScheduledDrugsFragmentView(scheduledDrugsFragment: ScheduledDrugsFragment): ScheduledDrugsFragmentView {
        return scheduledDrugsFragment
    }

    @Provides
    internal fun provideScheduledDrugsFragmentPresenter(scheduledDrugsFragmentView: ScheduledDrugsFragmentView, tasksRepository: TasksRepository) : ScheduledDrugsFragmentPresenter {
        return ScheduledDrugsFragmentPresenter(scheduledDrugsFragmentView, tasksRepository)
    }
}