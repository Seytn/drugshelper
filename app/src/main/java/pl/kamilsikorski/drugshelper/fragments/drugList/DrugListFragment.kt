package pl.kamilsikorski.drugshelper.fragments.drugList


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_drug_list.*
import pl.kamilsikorski.drugshelper.R
import pl.kamilsikorski.drugshelper.activities.addOrEditDrug.AddOrEditDrugActivity
import pl.kamilsikorski.drugshelper.adapters.DrugsListAdapter
import pl.kamilsikorski.drugshelper.adapters.ListItemClickListener
import pl.kamilsikorski.drugshelper.fragments.BaseFragment
import pl.kamilsikorski.drugshelper.models.drug.Drug

class DrugListFragment : BaseFragment<DrugListFragmentPresenter>(), DrugListFragmentView, ListItemClickListener {

    private lateinit var drugsListAdapter: DrugsListAdapter

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initItemTouchHelper()
        initBroadcastReceiver()

        presenter.loadDrugs()
    }

    private fun initRecyclerView() {
        drugsListAdapter = DrugsListAdapter(this)
        val layoutManager = LinearLayoutManager(context)

        rv_drug_list.layoutManager = layoutManager
        rv_drug_list.setHasFixedSize(true)
        rv_drug_list.adapter = drugsListAdapter
        rv_drug_list.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
    }

    private fun initItemTouchHelper() {
        ItemTouchHelper(object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                viewHolder?.let {
                    removeDrug(it.adapterPosition)
                }
            }

        }).attachToRecyclerView(rv_drug_list)
    }


    private fun loadEditDrugActivity(drug: Drug) {
        val drugProps = arrayOf(drug.name, drug.type.toString(), drug.note)
        val intent = Intent(activity, AddOrEditDrugActivity::class.java).apply {
            putExtra(AddOrEditDrugActivity.DRUG_PROPS_KEY, drugProps)
            putExtra(AddOrEditDrugActivity.DRUG_ID_KEY, drug.id)
        }
        startActivity(intent)
    }

    private fun initBroadcastReceiver() {
        LocalBroadcastManager.getInstance(activity).registerReceiver(reloadDrugsReceiver,
                IntentFilter("reload-drugs-event"))
    }

    private val reloadDrugsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            presenter.loadDrugs()
        }
    }

    fun removeDrug(adapterPosition: Int) {
        val drug = drugsListAdapter.drugList[adapterPosition]
        presenter.removeDrug(drug.id.toLong())
        drugsListAdapter.drugList.remove(drug)
        drugsListAdapter.notifyItemRemoved(adapterPosition)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_drug_list
    }

    override fun onListItemClick(itemIndex: Int) {
        val drug = drugsListAdapter.drugList[itemIndex]
        if (drug != null) {
            loadEditDrugActivity(drug)
        } else {
            displayError()
        }

    }

    override fun displayDrugs(drugList: List<Drug>) {
        drugsListAdapter.loadDrugList(drugList)
    }

    override fun displayNoDrugs() {
        Toast.makeText(context, R.string.found_no_books, Toast.LENGTH_SHORT).show()
    }

    override fun displayError() {
        Toast.makeText(context, R.string.error_occurred, Toast.LENGTH_SHORT).show()
    }

    override fun displayDrugRemoved() {
        Toast.makeText(context, R.string.drug_has_been_removed_from_the_list, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(reloadDrugsReceiver)
        super.onDestroy()
    }
}
