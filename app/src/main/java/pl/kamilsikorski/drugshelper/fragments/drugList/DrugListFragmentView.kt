package pl.kamilsikorski.drugshelper.fragments.drugList

import pl.kamilsikorski.drugshelper.models.drug.Drug

/**
 * Created by ksikorski on 13.10.2017.
 */
interface DrugListFragmentView {
    fun displayDrugs(drugList: List<Drug>)

    fun displayNoDrugs()

    fun displayDrugRemoved()

    fun displayError()
}