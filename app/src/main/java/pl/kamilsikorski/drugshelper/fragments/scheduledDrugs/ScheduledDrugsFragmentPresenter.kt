package pl.kamilsikorski.drugshelper.fragments.scheduledDrugs

import io.reactivex.schedulers.Schedulers
import pl.kamilsikorski.drugshelper.activities.BasePresenter
import pl.kamilsikorski.drugshelper.repositories.TasksRepository

/**
 * Created by ksikorski on 16.10.2017.
 */
class ScheduledDrugsFragmentPresenter(private var view: ScheduledDrugsFragmentView, private var tasksRepository: TasksRepository) : BasePresenter() {

    fun loadScheduledDrugs() {
        compositeDisposable.add(tasksRepository.getSchedule()
                .subscribeOn(Schedulers.io())
                .observeOn(mainScheduler)
                .subscribe({
                    if (it.isEmpty()) {
                        view.displayNoScheduledDrugs()
                    } else {
                        view.displaySchedule(it)
                    }
                },{
                    view.displayError()
                })
        )

    }


    fun markAsTakenScheduledDrug(id: Long, isTaken: Boolean) {
        compositeDisposable.add(tasksRepository.markTaskAsTaken(id, isTaken)
                .subscribeOn(Schedulers.io())
                .observeOn(mainScheduler)
                .subscribe({
                    if (it > 0) {
                        view.displayMarkedAsTaken()
                    } else {
                        view.displayError()
                    }
                }, {
                    view.displayError()
                } ))
    }
}