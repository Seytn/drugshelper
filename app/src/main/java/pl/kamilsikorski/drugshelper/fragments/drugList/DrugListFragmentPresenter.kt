package pl.kamilsikorski.drugshelper.fragments.drugList

import io.reactivex.schedulers.Schedulers
import pl.kamilsikorski.drugshelper.activities.BasePresenter
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository

/**
 * Created by ksikorski on 13.10.2017.
 */
class DrugListFragmentPresenter(private var view: DrugListFragmentView, private var drugsRepository: DrugsRepository) : BasePresenter() {

    fun loadDrugs() {
        compositeDisposable.add(drugsRepository.getDrugs()
                .subscribeOn(Schedulers.io())
                .observeOn(mainScheduler)
                .subscribe({
                    if (it.isEmpty()) {
                        view.displayNoDrugs()
                    } else {
                        view.displayDrugs(it)
                    }
                },{
                    view.displayError()
                })
        )

    }


    fun removeDrug(id: Long) {
        compositeDisposable.add(drugsRepository.removeDrug(id)
                .subscribeOn(Schedulers.io())
                .observeOn(mainScheduler)
                .subscribe({
                    if (it) {
                        view.displayDrugRemoved()
                    } else {
                        view.displayError()
                    }
                }, {
                    view.displayError()
                } ))
    }

}