package pl.kamilsikorski.drugshelper.fragments.drugList

import dagger.Module
import dagger.Provides
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository

/**
 * Created by ksikorski on 13.10.2017.
 */

@Module
class DrugListFragmentModule {

    @Provides
    internal fun provideDrugListFragmentView(drugListFragment: DrugListFragment) : DrugListFragmentView {
        return drugListFragment
    }

    @Provides
    internal fun provideDrugListFragmentPresenter(drugListFragmentView: DrugListFragmentView, drugsRepository: DrugsRepository): DrugListFragmentPresenter {
        return DrugListFragmentPresenter(drugListFragmentView, drugsRepository)
    }
}