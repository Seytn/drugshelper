package pl.kamilsikorski.drugshelper.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import pl.kamilsikorski.drugshelper.activities.BasePresenter
import pl.kamilsikorski.drugshelper.data.DbHelper
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository
import pl.kamilsikorski.drugshelper.repositories.TasksRepository
import javax.inject.Inject

/**
 * Created by ksikorski on 14.10.2017.
 */
abstract class BaseFragment<T: BasePresenter> : Fragment() {

    @Inject
    lateinit var presenter: T
    @Inject
    lateinit var drugsRepository: DrugsRepository
    @Inject
    lateinit var tasksRepository: TasksRepository
    @Inject
    lateinit var dbHelper: DbHelper

    protected abstract fun getLayoutId(): Int

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(getLayoutId(), container, false)
    }

    override fun onStop() {
        presenter.unsubscribe()
        super.onStop()
    }
}