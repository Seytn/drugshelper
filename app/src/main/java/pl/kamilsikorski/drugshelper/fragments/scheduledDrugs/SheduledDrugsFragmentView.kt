package pl.kamilsikorski.drugshelper.fragments.scheduledDrugs

import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask

/**
 * Created by ksikorski on 16.10.2017.
 */
interface ScheduledDrugsFragmentView {
    fun displayError()

    fun displayNoScheduledDrugs()

    fun displaySchedule(taskList: List<DrugTask>)

    fun displayMarkedAsTaken()
}
