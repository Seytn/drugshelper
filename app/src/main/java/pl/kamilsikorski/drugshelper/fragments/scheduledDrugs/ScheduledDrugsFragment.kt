package pl.kamilsikorski.drugshelper.fragments.scheduledDrugs

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_scheduled_drugs.*
import pl.kamilsikorski.drugshelper.R
import pl.kamilsikorski.drugshelper.adapters.DrugTaskListAdapter
import pl.kamilsikorski.drugshelper.adapters.ListItemClickListener
import pl.kamilsikorski.drugshelper.fragments.BaseFragment
import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask

/**
 * Created by ksikorski on 14.10.2017.
 */
class ScheduledDrugsFragment : BaseFragment<ScheduledDrugsFragmentPresenter>(), ScheduledDrugsFragmentView, ListItemClickListener {
    private lateinit var scheduledDrugListAdapter: DrugTaskListAdapter

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        presenter.loadScheduledDrugs()
    }

    private fun initRecyclerView() {
        scheduledDrugListAdapter = DrugTaskListAdapter(this)
        val layoutManager = LinearLayoutManager(context)

        rv_schedule.layoutManager = layoutManager
        rv_schedule.setHasFixedSize(true)
        rv_schedule.adapter = scheduledDrugListAdapter
        rv_schedule.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
    }

    override fun displayNoScheduledDrugs() {
        if (activity.viewPager.currentItem == 0)
        Toast.makeText(context, getString(R.string.drug_scheduler_is_empty), Toast.LENGTH_SHORT).show()
    }

    override fun displaySchedule(taskList: List<DrugTask>) {
        scheduledDrugListAdapter.loadTaskList(taskList)
    }

    override fun displayMarkedAsTaken() {
        Toast.makeText(context, getString(R.string.marked_as_taken), Toast.LENGTH_SHORT).show()
    }

    override fun getLayoutId(): Int = R.layout.fragment_scheduled_drugs

    override fun onListItemClick(itemIndex: Int) {
        scheduledDrugListAdapter.taskList[itemIndex].let {
            //TODO: remove message
            Toast.makeText(context, it.drug.name + " taken status changed to: " + !it.isTaken, Toast.LENGTH_SHORT).show()
            presenter.markAsTakenScheduledDrug(it.id.toLong(), !it.isTaken)
        }

    }

    override fun displayError() {
        Toast.makeText(context, R.string.error_occurred, Toast.LENGTH_SHORT).show()
    }


}