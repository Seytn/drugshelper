package pl.kamilsikorski.drugshelper.repositories

import io.reactivex.Single
import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drug.DrugType

/**
 * Created by ksikorski on 23.08.2017.
 */

interface DrugsRepository {
    fun getDrugs(): Single<List<Drug>>
    fun addNewDrug(name: String, type: DrugType, note: String): Single<Long>
    fun updateDrug(id: Long, name: String, type: DrugType, note: String): Single<Int>
    fun removeDrug(id: Long): Single<Boolean>
}
