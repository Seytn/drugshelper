package pl.kamilsikorski.drugshelper.repositories.impl

import io.reactivex.Single
import pl.kamilsikorski.drugshelper.data.DbHelper
import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drugTask.DosageType
import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask
import pl.kamilsikorski.drugshelper.repositories.TasksRepository
import java.sql.Time
import java.util.*

/**
 * Created by ksikorski on 16.10.2017.
 */
class TasksRepositoryImpl(private var dbHelper: DbHelper): TasksRepository {
    override fun getSchedule(): Single<List<DrugTask>> {
        return Single.fromCallable{ dbHelper.getAllDrugTasks() }
    }

    override fun addNewTask(drug: Drug, dosage: DosageType, interval: Time?, dosageHours: List<Date>?, nextDose: Date?): Single<Long> {
        return Single.fromCallable{ dbHelper.addNewDrugTask(drug, dosage, interval, dosageHours, nextDose) }
    }

    override fun updateTask(id: Long, drug: Drug, dosage: DosageType, interval: Time?, dosageHours: List<Date>?, nextDose: Date?, isTaken: Boolean): Single<Int> {
        return Single.fromCallable{ dbHelper.updateDrugTask(id, drug, dosage, interval, dosageHours, nextDose, isTaken) }
    }

    override fun markTaskAsTaken(id: Long, isTaken: Boolean): Single<Int> {
        return Single.fromCallable{ dbHelper.markDrugTaskAsTaken(id, isTaken)}
    }

    override fun removeTask(id: Long): Single<Boolean> {
        return Single.fromCallable{ dbHelper.removeDrug(id)}
    }

}