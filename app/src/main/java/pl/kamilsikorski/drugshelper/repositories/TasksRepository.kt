package pl.kamilsikorski.drugshelper.repositories

import io.reactivex.Single
import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drugTask.DosageType
import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask
import java.sql.Time
import java.util.*

/**
 * Created by ksikorski on 16.10.2017.
 */
interface TasksRepository {

    fun getSchedule(): Single<List<DrugTask>>

    fun addNewTask(drug: Drug,
                   dosage: DosageType,
                   interval: Time? = null,
                   dosageHours: List<Date>? = null,
                   nextDose: Date? = null): Single<Long>

    fun updateTask(id: Long, drug: Drug,
                   dosage: DosageType,
                   interval: Time? = null,
                   dosageHours: List<Date>? = null,
                   nextDose: Date? = null,
                   isTaken: Boolean): Single<Int>

    fun markTaskAsTaken(id: Long, isTaken: Boolean): Single<Int>

    fun removeTask(id: Long): Single<Boolean>

}