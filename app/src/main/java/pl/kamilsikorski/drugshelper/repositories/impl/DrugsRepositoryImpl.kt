package pl.kamilsikorski.drugshelper.repositories.impl

import io.reactivex.Single
import pl.kamilsikorski.drugshelper.data.DbHelper
import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drug.DrugType
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository

/**
 * Created by ksikorski on 23.08.2017.
 */

class DrugsRepositoryImpl(private var dbHelper: DbHelper) : DrugsRepository {

    override fun getDrugs(): Single<List<Drug>> {
        return Single.fromCallable { dbHelper.getAllDrugs() }
    }

    override fun addNewDrug(name: String, type: DrugType, note: String): Single<Long> {
        return Single.fromCallable { dbHelper.addNewDrug(name, type, note) }
    }

    override fun updateDrug(id: Long, name: String, type: DrugType, note: String): Single<Int> {
        return Single.fromCallable { dbHelper.updateDrug(id, name, type, note) }
    }

    override fun removeDrug(id: Long): Single<Boolean> {
        return Single.fromCallable { dbHelper.removeDrug(id) }
    }

}
