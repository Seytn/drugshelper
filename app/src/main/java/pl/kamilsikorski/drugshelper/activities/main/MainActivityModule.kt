package pl.kamilsikorski.drugshelper.activities.main

import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by ksikorski on 31.08.2017.
 */

@Module
class MainActivityModule {

    @Provides
    internal fun provideMainView(mainActivity: MainActivity): MainActivityView {
        return mainActivity
    }

    @Provides
    internal fun provideMainActivityPresenter(mainActivityView: MainActivityView): MainActivityPresenter {
        return MainActivityPresenter(mainActivityView, AndroidSchedulers.mainThread())
    }
}
