package pl.kamilsikorski.drugshelper.activities.addOrEditDrug

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.kamilsikorski.drugshelper.activities.BasePresenter
import pl.kamilsikorski.drugshelper.models.drug.DrugType
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository

/**
 * Created by ksikorski on 09.10.2017.
 */

class AddOrEditDrugActivityPresenter(private val view: AddOrEditDrugActivityView, private val drugsRepository: DrugsRepository) : BasePresenter() {

    internal fun saveDrug(id: Int, name: String, type: String, note: String) {
        if (name.isEmpty()) {
            view.displayDrugNameIsRequired()
            return
        }
        val drugType = DrugType.valueOf(type.toUpperCase())

        if (id > -1) {
            editDrug(id.toLong(), name, drugType, note)
        } else {
            addNewDrug(name, drugType, note)
        }

        view.closeActivity()
    }

    private fun addNewDrug(name: String, type: DrugType, note: String) {
        compositeDisposable.add(drugsRepository.addNewDrug(name, type, note)
                .subscribeOn(Schedulers.io())
                .observeOn(mainScheduler)
                .subscribe({
                    val errorCode: Long = -1
                    if (it == errorCode) {
                        view.displayError()
                    } else {
                        view.displayDrugAdded(it, name)
                        view.sendBroadcast()
                    }
                },{

                }))
    }

    private fun editDrug(id: Long, name: String, type: DrugType, note: String) {
        compositeDisposable.add(drugsRepository.updateDrug(id, name, type, note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it <= 0) {
                        view.displayError()
                    } else {
                        view.displayDrugUpdated(name)
                        view.sendBroadcast()
                    }
                },{
                    view.displayError()
                }))
    }

}
