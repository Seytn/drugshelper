package pl.kamilsikorski.drugshelper.activities

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by ksikorski on 09.10.2017.
 */

abstract class BasePresenter {

    var compositeDisposable = CompositeDisposable()
    var mainScheduler = AndroidSchedulers.mainThread()

    init {
        compositeDisposable = CompositeDisposable()
    }

    fun unsubscribe() {
        compositeDisposable.clear()
    }
}
