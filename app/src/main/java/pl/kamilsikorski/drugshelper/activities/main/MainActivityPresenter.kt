package pl.kamilsikorski.drugshelper.activities.main

import io.reactivex.Scheduler
import pl.kamilsikorski.drugshelper.activities.BasePresenter

/**
 * Created by ksikorski on 22.08.2017.
 */

class MainActivityPresenter : BasePresenter {

    private var view: MainActivityView

    //mainScheduler needed for tests passing.
    constructor(view: MainActivityView, mainScheduler: Scheduler) {
        this.view = view
        this.mainScheduler = mainScheduler
    }




}
