package pl.kamilsikorski.drugshelper.activities.addOrEditDrug

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_add_or_edit_drug.*
import kotlinx.android.synthetic.main.content_add_or_edit_drug.*
import pl.kamilsikorski.drugshelper.R
import pl.kamilsikorski.drugshelper.activities.BaseActivity
import pl.kamilsikorski.drugshelper.models.drug.DrugType

class AddOrEditDrugActivity : BaseActivity<AddOrEditDrugActivityPresenter>(), AddOrEditDrugActivityView {

    override fun getLayoutId(): Int = R.layout.activity_add_or_edit_drug

    // Value -1 means new
    private var drugId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val intent = intent
        drugId = intent.getIntExtra(DRUG_ID_KEY, -1)

        loadFields(intent)
        initFab()
    }

    private fun loadFields(intent: Intent) {
        if (drugId > -1) {
            val drugProps = intent.getStringArrayExtra(DRUG_PROPS_KEY)

            val name = drugProps[0]
            val type = drugProps[1]
            val note = drugProps[2]

            et_drug_name.setText(name)
            sp_drug_type.setSelection(DrugType.valueOf(type.toUpperCase()).index)
            et_drug_note.setText(note)
        }
    }

    private fun initFab() {
        fab.setOnClickListener { _ -> saveDrugAction() }
    }

    override fun displayDrugAdded(id: Long, drugName: String) {
        Toast.makeText(this, drugName + " " + id + " " + getString(R.string.has_been_added), Toast.LENGTH_SHORT).show()
    }

    override fun displayDrugUpdated(drugName: String) {
        Toast.makeText(this, drugName + " " + getString(R.string.has_been_updated), Toast.LENGTH_SHORT).show()
    }

    override fun sendBroadcast() {
        val intent = Intent("reload-drugs-event")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun displayError() {
        Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show()
    }

    override fun displayError(errorMsg: Int) {
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show()
    }

    override fun displayDrugNameIsRequired() {
        Toast.makeText(this, R.string.name_is_required, Toast.LENGTH_LONG).show()
    }

    override fun closeActivity() {
        finish()
    }

    private fun saveDrugAction() {
        val name = et_drug_name.text.toString()
        val type = sp_drug_type.selectedItem.toString()
        val note = et_drug_note.text.toString()
        presenter.saveDrug(drugId, name, type, note)
    }

    companion object {

        val DRUG_PROPS_KEY = "drugToEdit"
        val DRUG_ID_KEY = "drugID"
    }

}
