package pl.kamilsikorski.drugshelper.activities.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import pl.kamilsikorski.drugshelper.R
import pl.kamilsikorski.drugshelper.activities.BaseActivity
import pl.kamilsikorski.drugshelper.activities.addOrEditDrug.AddOrEditDrugActivity
import pl.kamilsikorski.drugshelper.adapters.SectionsStatePagerAdapter
import pl.kamilsikorski.drugshelper.fragments.drugList.DrugListFragment
import pl.kamilsikorski.drugshelper.fragments.scheduledDrugs.ScheduledDrugsFragment
import javax.inject.Inject

class MainActivity : BaseActivity<MainActivityPresenter>(), MainActivityView, HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    lateinit var sectionsStatePagerAdapter: SectionsStatePagerAdapter

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        initBottomNav()
        initViewPager()
        initFab()
    }

    private fun initBottomNav() {
        bottom_nav.setOnNavigationItemSelectedListener { item ->
            var tab = 0
            when (item.itemId) {
                R.id.menu_scheduler -> tab = 0
                R.id.menu_drug_list -> tab = 1
                R.id.menu_tba -> tab = 2
            }
            selectPage(tab)
            true
        }

    }

    private fun initViewPager() {
        sectionsStatePagerAdapter = SectionsStatePagerAdapter(supportFragmentManager)
        sectionsStatePagerAdapter.addFragment(ScheduledDrugsFragment(), "Scheduled")
        sectionsStatePagerAdapter.addFragment(DrugListFragment(), "Drug List")
        sectionsStatePagerAdapter.addFragment(Fragment(), "Empty")
        viewPager.adapter = sectionsStatePagerAdapter
        //TODO: remove next two lines - only for development
        viewPager.currentItem = 1
        bottom_nav.selectedItemId = R.id.menu_drug_list
        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                when(position) {
                    0 -> bottom_nav.selectedItemId = R.id.menu_scheduler
                    1 -> bottom_nav.selectedItemId = R.id.menu_drug_list
                    2 -> bottom_nav.selectedItemId = R.id.menu_tba
                }
            }
        })
    }

    private fun initFab() {
        fab.setOnClickListener { fabAction() }
    }

    private fun fabAction() {
        when (viewPager.currentItem) {
            1 -> {
                val intent = Intent(this, AddOrEditDrugActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun selectPage(pageId : Int) {
        viewPager.currentItem = pageId
    }
}
