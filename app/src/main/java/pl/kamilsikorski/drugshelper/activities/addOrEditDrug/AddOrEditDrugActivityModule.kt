package pl.kamilsikorski.drugshelper.activities.addOrEditDrug

import dagger.Module
import dagger.Provides
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository

/**
 * Created by ksikorski on 09.10.2017.
 */

@Module
class AddOrEditDrugActivityModule {
    @Provides
    internal fun provideAddOrEditDrugView(addOrEditDrugActivity: AddOrEditDrugActivity): AddOrEditDrugActivityView {
        return addOrEditDrugActivity
    }

    @Provides
    internal fun provideAddOrEditDrugActivityPresenter(addOrEditDrugActivityView: AddOrEditDrugActivityView, drugsRepository: DrugsRepository): AddOrEditDrugActivityPresenter {
        return AddOrEditDrugActivityPresenter(addOrEditDrugActivityView, drugsRepository)
    }
}
