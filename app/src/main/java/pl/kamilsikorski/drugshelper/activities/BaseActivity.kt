package pl.kamilsikorski.drugshelper.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import pl.kamilsikorski.drugshelper.data.DbHelper
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository
import javax.inject.Inject

/**
 * Created by ksikorski on 04.09.2017.
 */

abstract class BaseActivity<T: BasePresenter> : AppCompatActivity() {

    lateinit var sharedPreferences: SharedPreferences
    @Inject
    lateinit var presenter: T
    @Inject
    lateinit var drugsRepository: DrugsRepository
    @Inject
    lateinit var dbHelper: DbHelper

    protected abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(getLayoutId())
        super.onCreate(savedInstanceState)

        initToolbar()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        if (this.parentActivityIntent != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }


    override fun onStop() {
        presenter.unsubscribe()
        super.onStop()
    }
}
