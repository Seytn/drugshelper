package pl.kamilsikorski.drugshelper.activities.addOrEditDrug

/**
 * Created by ksikorski on 09.10.2017.
 */

interface AddOrEditDrugActivityView {

    fun displayError()
    fun displayError(errorMsg: Int)

    fun closeActivity()

    fun displayDrugAdded(id: Long, drugName: String)
    fun displayDrugUpdated(drugName: String)

    fun sendBroadcast()
    fun displayDrugNameIsRequired()
}
