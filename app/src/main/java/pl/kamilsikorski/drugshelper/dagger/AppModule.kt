package pl.kamilsikorski.drugshelper.dagger

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import pl.kamilsikorski.drugshelper.data.DbHelper
import javax.inject.Singleton

/**
 * Created by ksikorski on 26.08.2017.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(application: Application): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideDBHelper(context: Context): DbHelper {
        return DbHelper(context)
    }

    @Provides
    @Singleton
     fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("DHSP", Context.MODE_PRIVATE)
    }
}
