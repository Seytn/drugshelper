package pl.kamilsikorski.drugshelper.dagger

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import pl.kamilsikorski.drugshelper.DrugsHelperApplication
import javax.inject.Singleton

/**
 * Created by ksikorski on 26.08.2017.
 */
@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, AppModule::class, ActivityBuilder::class, FragmentBuilder::class, RepositoryModule::class))
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: DrugsHelperApplication)

}
