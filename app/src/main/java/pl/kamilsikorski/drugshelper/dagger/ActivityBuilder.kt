package pl.kamilsikorski.drugshelper.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.kamilsikorski.drugshelper.activities.addOrEditDrug.AddOrEditDrugActivity
import pl.kamilsikorski.drugshelper.activities.addOrEditDrug.AddOrEditDrugActivityModule
import pl.kamilsikorski.drugshelper.activities.main.MainActivity
import pl.kamilsikorski.drugshelper.activities.main.MainActivityModule

/**
 * Created by ksikorski on 31.08.2017.
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun bindDrugsActivity(): MainActivity

    @ContributesAndroidInjector(modules = arrayOf(AddOrEditDrugActivityModule::class))
    abstract fun bindAddNewDrugActivity(): AddOrEditDrugActivity

}