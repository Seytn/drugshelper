package pl.kamilsikorski.drugshelper.dagger

import dagger.Module
import dagger.Provides
import pl.kamilsikorski.drugshelper.data.DbHelper
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository
import pl.kamilsikorski.drugshelper.repositories.TasksRepository
import pl.kamilsikorski.drugshelper.repositories.impl.DrugsRepositoryImpl
import pl.kamilsikorski.drugshelper.repositories.impl.TasksRepositoryImpl
import javax.inject.Singleton

/**
 * Created by ksikorski on 27.08.2017.
 */

@Module
class RepositoryModule {

    @Provides
    @Singleton
     fun provideDrugsRepository(dbHelper: DbHelper): DrugsRepository {
        return DrugsRepositoryImpl(dbHelper)
    }

    @Provides
    @Singleton
    fun provideTasksRepository(dbHelper: DbHelper): TasksRepository {
        return TasksRepositoryImpl(dbHelper)
    }
}
