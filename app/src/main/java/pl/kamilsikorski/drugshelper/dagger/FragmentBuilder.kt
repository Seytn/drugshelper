package pl.kamilsikorski.drugshelper.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.kamilsikorski.drugshelper.fragments.drugList.DrugListFragment
import pl.kamilsikorski.drugshelper.fragments.drugList.DrugListFragmentModule
import pl.kamilsikorski.drugshelper.fragments.scheduledDrugs.ScheduledDrugsFragment
import pl.kamilsikorski.drugshelper.fragments.scheduledDrugs.ScheduledDrugsFragmentModule

/**
 * Created by ksikorski on 13.10.2017.
 */

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = arrayOf(DrugListFragmentModule::class))
    abstract fun bindDrugListFragment(): DrugListFragment

    @ContributesAndroidInjector(modules = arrayOf(ScheduledDrugsFragmentModule::class))
    abstract fun bindScheduledDrugsFragment(): ScheduledDrugsFragment


}
