package pl.kamilsikorski.drugshelper.models.drug

/**
 * Created by ksikorski on 23.08.2017.
 */

class Drug {

    var id: Int
    var name: String
    var type: DrugType
    var note: String

    constructor(id: Int, name: String, type: DrugType, note: String) {
        this.id = id
        this.name = name
        this.type = type
        this.note = note
    }

    constructor(name: String, type: DrugType, note: String) {
        this.id = -1
        this.name = name
        this.type = type
        this.note = note
    }
}
