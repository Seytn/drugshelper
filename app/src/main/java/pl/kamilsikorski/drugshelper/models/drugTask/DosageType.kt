package pl.kamilsikorski.drugshelper.models.drugTask

/**
 * Created by ksikorski on 16.10.2017.
 */
enum class DosageType(private val stringValue: String, val index: Int) {
    ONE_TIME("One time", 0),
    INTERVAL("Interval", 1),
    CUSTOM("Custom", 2);

    override fun toString(): String {
        return stringValue
    }

}