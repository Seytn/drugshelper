package pl.kamilsikorski.drugshelper.models.drug

/**
 * Created by ksikorski on 07.09.2017.
 */


enum class DrugType(private val stringValue: String, val index: Int) {
    PILL("Pill", 0),
    TABLET("Tablet", 1),
    INJECTION("Injection", 2),
    SUPPOSITORY("Suppository", 3),
    OTHER("Other", 4);

    override fun toString(): String {
        return stringValue
    }
}

