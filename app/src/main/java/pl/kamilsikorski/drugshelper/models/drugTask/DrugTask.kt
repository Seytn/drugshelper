package pl.kamilsikorski.drugshelper.models.drugTask

import pl.kamilsikorski.drugshelper.models.drug.Drug
import java.sql.Time
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by ksikorski on 16.10.2017.
 */
class DrugTask {

    var id: Int = 0
    var drug: Drug
    var dosage: DosageType
    var interval: Time? = null
    var dosageHours: List<Date>? = null
    var nextDose: Date? = null
    var isTaken: Boolean = false

    constructor(id: Int = 0, drug: Drug, dosage: DosageType, interval: Time? = null, dosageHours: List<Date>? = null, nextDose: Date? = null, isTaken: Boolean = false)
    {
        this.id = id
        this.drug = drug
        this.dosage = dosage
        this.interval = interval
        this.dosageHours = dosageHours
        this.nextDose = nextDose
        this.isTaken = isTaken
    }
    constructor(id: Int, drug: Drug, dosage: DosageType, interval: Long?, dosageHours: String?, nextDose: Long?, isTaken: Boolean) {
        this.id = id
        this.drug = drug
        this.dosage = dosage
        if (interval !=null) {
            this.interval = Time(interval)
        }
        this.dosageHours = dosageHoursToList(dosageHours)
        if (nextDose !=null) {
            this.nextDose = Date(nextDose)
        }
        this.isTaken = isTaken
    }

    private fun dosageHoursToList(dosageHoursRaw: String?): List<Date>? {
        if (dosageHoursRaw == null || dosageHoursRaw.isEmpty()) {
            return null
        }

        val dosageHours = ArrayList<Date>()
        val split = dosageHoursRaw.split(",")
        split.mapTo(dosageHours) { Date(it.toLong()) }
        return dosageHours
    }

    companion object {
        fun dosageHoursListToString(dosageHours: List<Date>?): String? {
            if (dosageHours == null || dosageHours.isEmpty()) {
                return null
            }

            val dosageHoursString = StringBuilder()
            for (hour in dosageHours) {
                dosageHoursString.append(hour.time.toString() + ",")
            }
            dosageHoursString.removeSuffix(",")
            return dosageHoursString.toString()
        }
    }

}