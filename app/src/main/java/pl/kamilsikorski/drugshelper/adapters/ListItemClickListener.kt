package pl.kamilsikorski.drugshelper.adapters

/**
 * Created by ksikorski on 31.08.2017.
 */


interface ListItemClickListener {
    fun onListItemClick(itemIndex: Int)
}
