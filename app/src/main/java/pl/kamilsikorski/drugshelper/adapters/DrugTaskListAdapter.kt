package pl.kamilsikorski.drugshelper.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.scheduled_drugs_item.view.*
import pl.kamilsikorski.drugshelper.R
import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask
import java.util.*

/**
 * Created by ksikorski on 26.10.2017.
 */
class DrugTaskListAdapter(private val listItemClickListener: ListItemClickListener): RecyclerView.Adapter<DrugTaskListAdapter.DrugTaskViewHolder>() {
    val taskList = ArrayList<DrugTask>()

    fun loadTaskList(taskList: List<DrugTask>) {
        this.taskList.clear()
        this.taskList.addAll(0, taskList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrugTaskViewHolder {
        return DrugTaskViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.scheduled_drugs_item, parent, false))
    }

    override fun onBindViewHolder(holder: DrugTaskViewHolder, position: Int) {
        holder.listItemClickListener = listItemClickListener
        holder.itemView.tv_drug_name.text = taskList[position].drug.name
        holder.itemView.tv_dosage_time.text = taskList[position].nextDose.toString()
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    class DrugTaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        lateinit var listItemClickListener: ListItemClickListener

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val position = adapterPosition
            listItemClickListener.onListItemClick(position)
        }

    }

}