package pl.kamilsikorski.drugshelper.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pl.kamilsikorski.drugshelper.R;
import pl.kamilsikorski.drugshelper.models.drug.Drug;

public class DrugsListAdapter extends RecyclerView.Adapter<DrugsListAdapter.DrugViewHolder> {

    private final List<Drug> drugList = new ArrayList<>();
    final private ListItemClickListener onClickListener;

    public DrugsListAdapter(ListItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public List<Drug> getDrugList() {
        return drugList;
    }

    public void loadDrugList(List<Drug> drugList) {
        this.drugList.clear();
        this.drugList.addAll(0, drugList);
        notifyDataSetChanged();
    }

    public void insertNewItem(Drug drug) {
        int lastIndex = drugList.size();
        this.drugList.add(lastIndex, drug);
        notifyItemInserted(lastIndex);
    }

    @Override
    public DrugViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DrugViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.drug_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(DrugViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return drugList.size();
    }

    class DrugViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView listItemDrugNameView;

        public DrugViewHolder(View itemView) {
            super(itemView);
            listItemDrugNameView = (TextView) itemView.findViewById(R.id.tv_drug_name);

            itemView.setOnClickListener(this);
        }

        public void bind(int position) {
            String name = drugList.get(position).getName();
            listItemDrugNameView.setText(name);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            onClickListener.onListItemClick(position);
        }
    }
}
