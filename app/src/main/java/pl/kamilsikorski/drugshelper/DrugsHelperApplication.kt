package pl.kamilsikorski.drugshelper

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import pl.kamilsikorski.drugshelper.dagger.AppComponent
import pl.kamilsikorski.drugshelper.dagger.DaggerAppComponent
import javax.inject.Inject

/**
 * Created by ksikorski on 26.08.2017.
 */

class DrugsHelperApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    private val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
    }

    override fun onCreate() {
        super.onCreate()

        component.inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity>? =
            activityInjector
}
