package pl.kamilsikorski.drugshelper.data

import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drugTask.DosageType
import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask
import java.sql.Time
import java.util.*

/**
 * Created by ksikorski on 19.10.2017.
 */
interface DrugTaskDAO {
    fun getAllDrugTasks(): List<DrugTask>

    fun addNewDrugTask(drug: Drug,
                       dosage: DosageType,
                       interval: Time? = null,
                       dosageHours: List<Date>? = null,
                       nextDose: Date? = null): Long

    fun updateDrugTask(id: Long,
                       drug: Drug,
                       dosage: DosageType,
                       interval: Time? = null,
                       dosageHours: List<Date>? = null,
                       nextDose: Date? = null,
                       isTaken: Boolean): Int

    fun removeDrugTask(id: Long): Boolean

    fun markDrugTaskAsTaken(id: Long, isTaken: Boolean = true): Int
}