package pl.kamilsikorski.drugshelper.data

import android.provider.BaseColumns

/**
 * Created by ksikorski on 28.08.2017.
 */

object DrugsContract {

    class DrugsEntry {
        companion object {
            val TABLE_NAME = "drugsList"
            val COLUMN_DRUG_NAME = "drugName"
            val COLUMN_DRUG_TYPE = "drugType"
            val COLUMN_DRUG_NOTE = "drugNote"
            val ID = BaseColumns._ID
            val COUNT = BaseColumns._COUNT
        }
    }
}
