package pl.kamilsikorski.drugshelper.data

import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drug.DrugType

/**
 * Created by ksikorski on 31.08.2017.
 */

interface DrugDAO {
    fun getAllDrugs(): List<Drug>

    fun addNewDrug(name: String, type: DrugType, note: String): Long

    fun updateDrug(id: Long, name: String, type: DrugType, note: String): Int

    fun removeDrug(id: Long): Boolean
}
