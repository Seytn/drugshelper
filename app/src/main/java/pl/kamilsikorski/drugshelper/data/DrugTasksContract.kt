package pl.kamilsikorski.drugshelper.data

import android.provider.BaseColumns

/**
 * Created by ksikorski on 16.10.2017.
 */

object DrugTasksContract {

    class DrugTasksEntry {
        companion object {
            val TABLE_NAME = "drugTasksList"
            val COLUMN_DRUG_ID = "drugID"
            val COLUMN_DOSAGE = "dosage"
            val COLUMN_INTERVAL = "interval"
            val COLUMN_DOSAGE_HOURS = "dosageHours"
            val COLUMN_NEXT_DOSE = "nextDose"
            val COLUMN_IS_TAKEN = "isTaken"
            val ID = BaseColumns._ID
            val COUNT = BaseColumns._COUNT
        }
    }
}
