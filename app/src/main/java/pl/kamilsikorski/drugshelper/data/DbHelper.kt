package pl.kamilsikorski.drugshelper.data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import pl.kamilsikorski.drugshelper.data.DrugTasksContract.DrugTasksEntry
import pl.kamilsikorski.drugshelper.data.DrugsContract.DrugsEntry
import pl.kamilsikorski.drugshelper.models.drug.Drug
import pl.kamilsikorski.drugshelper.models.drug.DrugType
import pl.kamilsikorski.drugshelper.models.drugTask.DosageType
import pl.kamilsikorski.drugshelper.models.drugTask.DrugTask
import java.sql.SQLException
import java.sql.Time
import java.util.*

/**
 * Created by ksikorski on 30.08.2017.
 */

class DbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION), DrugDAO, DrugTaskDAO {

    override fun getAllDrugs(): List<Drug> {

        var cursor: Cursor? = null
        try {
            cursor = readableDatabase.query(
                    DrugsEntry.TABLE_NAME, null, null, null, null, null, null)

            val drugList = ArrayList<Drug>()
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                val id = cursor.getInt(cursor.getColumnIndex(DrugsEntry.ID))
                val name = cursor.getString(cursor.getColumnIndex(DrugsEntry.COLUMN_DRUG_NAME))
                val type = DrugType.valueOf(cursor.getString(cursor.getColumnIndex(DrugsEntry.COLUMN_DRUG_TYPE)).toUpperCase())
                val note = cursor.getString(cursor.getColumnIndex(DrugsEntry.COLUMN_DRUG_NOTE))

                drugList.add(Drug(id, name, type, note))
                cursor.moveToNext()
            }

            return drugList

        } catch ( error: SQLException ) {
            Log.e("", error.message)
            throw SQLException()
        } finally {
            cursor?.close()

        }
    }

    override fun addNewDrug(name: String, type: DrugType, note: String): Long {
        val cv = ContentValues()
        cv.put(DrugsEntry.COLUMN_DRUG_NAME, name)
        cv.put(DrugsEntry.COLUMN_DRUG_TYPE, type.toString())
        cv.put(DrugsEntry.COLUMN_DRUG_NOTE, note)
        return writableDatabase.insert(DrugsEntry.TABLE_NAME, null, cv)
    }

    override fun updateDrug(id: Long, name: String, type: DrugType, note: String): Int {
        val cv = ContentValues()
        cv.put(DrugsEntry.ID, id)
        cv.put(DrugsEntry.COLUMN_DRUG_NAME, name)
        cv.put(DrugsEntry.COLUMN_DRUG_TYPE, type.toString())
        cv.put(DrugsEntry.COLUMN_DRUG_NOTE, note)
        return writableDatabase.update(DrugsEntry.TABLE_NAME, cv, "?=?", arrayOf("_ID=", id.toString()))
    }

    override fun removeDrug(id: Long): Boolean {
        return writableDatabase.delete(DrugsEntry.TABLE_NAME,
                "?=?", arrayOf("_ID=", id.toString())) > 0
    }

    override fun getAllDrugTasks(): List<DrugTask> {
        var cursor: Cursor? = null
        try {
            val query = "SELECT * " +
                    "FROM " + DrugsEntry.TABLE_NAME + " AS d " +
                    "INNER JOIN " + DrugTasksEntry.TABLE_NAME + " AS t " +
                    "ON d." + DrugsEntry.ID + " = t." + DrugTasksEntry.COLUMN_DRUG_ID
            cursor = readableDatabase.rawQuery(query, null)

            val drugTaskList = ArrayList<DrugTask>()
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                val id = cursor.getInt(cursor.getColumnIndex(DrugTasksEntry.ID))
                val dosage = DosageType.valueOf(cursor.getString(cursor.getColumnIndex(DrugTasksEntry.COLUMN_DOSAGE)).toUpperCase())
                val interval = cursor.getLong(cursor.getColumnIndex(DrugTasksEntry.COLUMN_INTERVAL))
                val dosageHours = cursor.getString(cursor.getColumnIndex(DrugTasksEntry.COLUMN_DOSAGE_HOURS))
                val nextDose = cursor.getLong(cursor.getColumnIndex(DrugTasksEntry.COLUMN_NEXT_DOSE))
                val isTaken = cursor.getInt(cursor.getColumnIndex(DrugTasksEntry.COLUMN_IS_TAKEN)) > 0

                val drugId = cursor.getInt(cursor.getColumnIndex(DrugTasksEntry.COLUMN_DRUG_ID))
                val drugName = cursor.getString(cursor.getColumnIndex(DrugsEntry.COLUMN_DRUG_NAME))
                val drugType = DrugType.valueOf(cursor.getString(cursor.getColumnIndex(DrugsEntry.COLUMN_DRUG_TYPE)).toUpperCase())
                val drugNote = cursor.getString(cursor.getColumnIndex(DrugsEntry.COLUMN_DRUG_NOTE))

                drugTaskList.add(DrugTask(id,
                        Drug(drugId, drugName, drugType, drugNote),
                        dosage,
                        interval,
                        dosageHours,
                        nextDose,
                        isTaken))
                cursor.moveToNext()
            }

            return drugTaskList

        } finally {
            cursor?.close()
        }

    }

    override fun addNewDrugTask(drug: Drug, dosage: DosageType, interval: Time?, dosageHours: List<Date>?, nextDose: Date?): Long {
        val cv = ContentValues()
        cv.put(DrugTasksEntry.COLUMN_DRUG_ID, drug.id)
        cv.put(DrugTasksEntry.COLUMN_DOSAGE, dosage.toString())
        cv.put(DrugTasksEntry.COLUMN_INTERVAL, interval?.time)
        cv.put(DrugTasksEntry.COLUMN_DOSAGE_HOURS, DrugTask.dosageHoursListToString(dosageHours))
        cv.put(DrugTasksEntry.COLUMN_NEXT_DOSE, nextDose?.time)
        cv.put(DrugTasksEntry.COLUMN_IS_TAKEN, false)
        return writableDatabase.insert(DrugTasksEntry.TABLE_NAME, null, cv)
    }

    override fun updateDrugTask(id: Long, drug: Drug, dosage: DosageType, interval: Time?, dosageHours: List<Date>?, nextDose: Date?, isTaken: Boolean): Int {
        val cv = ContentValues()
        cv.put(DrugTasksEntry.ID, id)
        cv.put(DrugTasksEntry.COLUMN_DRUG_ID, drug.id)
        cv.put(DrugTasksEntry.COLUMN_DOSAGE, dosage.toString())
        cv.put(DrugTasksEntry.COLUMN_INTERVAL, interval?.time)
        cv.put(DrugTasksEntry.COLUMN_DOSAGE_HOURS, DrugTask.dosageHoursListToString(dosageHours))
        cv.put(DrugTasksEntry.COLUMN_NEXT_DOSE, nextDose?.time)
        cv.put(DrugTasksEntry.COLUMN_IS_TAKEN, isTaken)
        return writableDatabase.update(DrugTasksEntry.TABLE_NAME, cv, "?=?", arrayOf("_ID=", id.toString()))
    }

    override fun removeDrugTask(id: Long): Boolean {
        return writableDatabase.delete(DrugTasksEntry.TABLE_NAME,
                "_ID=" + id, null) > 0
    }

    override fun markDrugTaskAsTaken(id: Long, isTaken: Boolean): Int {
        val cv = ContentValues()
        cv.put(DrugTasksEntry.COLUMN_IS_TAKEN, isTaken)
        return writableDatabase.update(DrugTasksEntry.TABLE_NAME, cv, "?=?", arrayOf("_ID=", id.toString()))
//         writableDatabase.rawQuery("UPDATE ? SET ? = ? WHERE _ID= ?", arrayOf(DrugTasksEntry.TABLE_NAME, DrugTasksEntry.COLUMN_IS_TAKEN, isTaken, id.toString()))
    }

    override fun onCreate(db: SQLiteDatabase) {
        val SQL_CREATE_DRUGS_LIST_TABLE = "CREATE TABLE " +
                DrugsEntry.TABLE_NAME + " (" +
                DrugsEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DrugsEntry.COLUMN_DRUG_NAME + " TEXT NOT NULL, " +
                DrugsEntry.COLUMN_DRUG_TYPE + " TEXT NOT NULL, " +
                DrugsEntry.COLUMN_DRUG_NOTE + " TEXT" +
                ");"

        val SQL_CREATE_TASKS_LIST_TABLE = "CREATE TABLE " +
                DrugTasksEntry.TABLE_NAME + " (" +
                DrugTasksEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DrugTasksEntry.COLUMN_DRUG_ID + " INTEGER, " +
                DrugTasksEntry.COLUMN_DOSAGE + " TEXT NOT NULL, " +
                DrugTasksEntry.COLUMN_INTERVAL + " TEXT, " +
                DrugTasksEntry.COLUMN_DOSAGE_HOURS + " TEXT, " +
                DrugTasksEntry.COLUMN_NEXT_DOSE + " TEXT, " +
                "FOREIGN KEY(" + DrugTasksEntry.COLUMN_DRUG_ID + ") REFERENCES " + DrugsEntry.TABLE_NAME + " (" + DrugsEntry.ID + ") " +
                ");"

        db.execSQL(SQL_CREATE_DRUGS_LIST_TABLE)
        db.execSQL(SQL_CREATE_TASKS_LIST_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //TODO upgrade
        db.execSQL("DROP TABLE IF EXISTS " + DrugsEntry.TABLE_NAME)
        db.execSQL("DROP TABLE IF EXISTS " + DrugTasksEntry.TABLE_NAME)
        onCreate(db)
    }

    companion object {

        private val DATABASE_NAME = "drugs_helper.db"

        private val DATABASE_VERSION = 4
    }
}
