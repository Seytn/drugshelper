package pl.kamilsikorski.drugshelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;

import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import pl.kamilsikorski.drugshelper.activities.main.MainActivityPresenter;
import pl.kamilsikorski.drugshelper.activities.main.MainActivityView;
import pl.kamilsikorski.drugshelper.models.drug.Drug;
import pl.kamilsikorski.drugshelper.repositories.DrugsRepository;

/**
 * Created by ksikorski on 23.08.2017.
 */

public class MainActivityPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    DrugsRepository drugsRepository;

    @Mock
    MainActivityView view;

    @Mock
    Drug drug1;

    @Mock
    Drug drug2;

    @Mock
    Drug drug3;

    private MainActivityPresenter presenter;

    private final List<Drug> DRUG_LIST = Arrays.asList(drug1, drug2, drug3);

    @Before
    public void setUp() {
        presenter = new MainActivityPresenter(view, Schedulers.trampoline());
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    @After
    public void cleanUp() {
        RxJavaPlugins.reset();
    }

//    @Test
//    public void shouldPassDrugsToView() {
//        when(drugsRepository.getDrugs()).thenReturn(Single.just(DRUG_LIST));
//
//        presenter.loadDrugs();
//
//        verify(view).displayDrugs(DRUG_LIST);    }
//
//    @Test
//    public void shouldHandleNoBooksFound() {
//        when(drugsRepository.getDrugs()).thenReturn(Single.just(Collections.emptyList()));
//
//        presenter.loadDrugs();
//
//        verify(view).displayNoDrugs();
//    }
//
//    @Test
//    public void  shouldHandleError() {
//        when(drugsRepository.getDrugs()).thenReturn(Single.error(new Throwable("error")));
//
//        presenter.loadDrugs();
//
//        verify(view).displayError();
//    }

}